import { DailyInterval } from "./daily-interval";

export interface Schedule {
    id: string;
    userId: string;
    intervals: DailyInterval[];
}
