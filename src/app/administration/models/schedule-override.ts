import { DailyInterval } from "./daily-interval";

export interface ScheduleOverride {
    id: string;
    userId: string;
    date: Date;
    interval: DailyInterval;
}
