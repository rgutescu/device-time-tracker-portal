import { DailyInterval } from "./daily-interval";

export interface DailyIntervalForms extends DailyInterval {
    startKey: string;
    endKey: string;
    allowedMinutesKey: string;
}
