export interface ScheduleOverrideCriteria {
    userId: string;
    date: string;
}
