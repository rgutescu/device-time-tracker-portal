export interface DailyInterval {
    day?: number;
    startTime: string;
    endTime: string;
    allowedMinutes: number;
}
