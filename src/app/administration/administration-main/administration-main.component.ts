import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import {  filter } from 'rxjs/operators';
import { UserWorkstation } from 'src/app/activity/models/user-workstation';
import { AppState } from 'src/app/state/models/app-state';

import * as userActions from '../../state/actions/user.actions';
import * as scheduleActions from '../../state/actions/schedule.actions';
import { Schedule } from '../models/schedule';
import { ScheduleOverride } from '../models/schedule-override';
import { ServerOperationStatus } from 'src/app/state/models/server-operation-status.enum';
import { Router } from '@angular/router';

import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { getTodayDateWithoutTime } from 'src/app/core/helpers/date-utils';

@Component({
  selector: 'trk-administration-main',
  templateUrl: './administration-main.component.html',
  styleUrls: ['./administration-main.component.scss']
})
export class AdministrationMainComponent implements OnInit, OnDestroy {

  isLargeResolution = false;
  userId: string = "";
  userWorkstations$: Observable<UserWorkstation[]>;
  isUserDataLoading$: Observable<ServerOperationStatus>;
  scheduleOverride$: Observable<ScheduleOverride>;
  schedule$: Observable<Schedule>;

  loadScheduleSubscripion: Subscription;
  serverOperationInProgressStatus = ServerOperationStatus.InProgress;

  private breakpointObserverLimit = '(min-width: 1000px)'; 
  constructor(
    private store: Store<AppState>,
    private router: Router,
    public breakpointObserver: BreakpointObserver
    
    ) { 
    this.userWorkstations$ = this.store.select(state => state.userData.items);
    this.isUserDataLoading$ = this.store.select(state => state.userData.severOperationtStatus);
    this.schedule$ = this.store.select(state => state.scheduleData.schedule);
    this.scheduleOverride$ = this.store.select(state => state.scheduleData.scheduleOverride);

    this.loadScheduleSubscripion = this.store
      .select(state => state.userData.selectedUser)
      .pipe (
        filter(u => u != null),
      )
      .subscribe(u => this.loadScheduleData(u.id));
  }

  ngOnInit(): void {  
    this.store.dispatch(new userActions.GetUserWorkstationsAction());

    this.breakpointObserver
      .observe([this.breakpointObserverLimit])
      .subscribe((state: BreakpointState) => {
        this.isLargeResolution = state.matches;
    });
  }

  ngOnDestroy() : void {
    this.loadScheduleSubscripion?.unsubscribe();
  }

  loadScheduleData(userId: string): void {
    this.userId = userId;
    this.store.dispatch(new scheduleActions.GetUserWeeklyScheduleAction(userId));
    this.store.dispatch(new scheduleActions.GetUserOverrideScheduleAction({userId: userId, date: getTodayDateWithoutTime().toISOString()}));
  }

  addUser(): void {
    this.router.navigate(['administration/add-user']);
  }

  editUser($event: string): void {
    this.router.navigate([`administration/edit-user/${$event}`]);
  }

  selectUser($event: UserWorkstation): void {
    this.store.dispatch(new userActions.SelectUserWorkstationsAction($event));
  }

  editSchedule($event: string): void {
    this.router.navigate([`administration/edit-schedule/${$event}`]);
  }

  editScheduleOverride($event: string): void {
    this.router.navigate([`administration/edit-schedule-override/${$event}`]);
  }
}
