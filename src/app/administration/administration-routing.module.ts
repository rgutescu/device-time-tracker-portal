import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdministrationMainComponent } from './administration-main/administration-main.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditWeekScheduleComponent } from './edit-week-schedule/edit-week-schedule.component';
import { EditDayScheduleOverrideComponent } from './edit-day-schedule-override/edit-day-schedule-override.component';

const routes: Routes = [
  {
      path: '',
      component: AdministrationMainComponent,
  },
  {
    path: 'add-user',
    component: EditUserComponent,
  },
  {
    path: 'edit-user/:id',
    component: EditUserComponent,
  },
  {
    path: 'edit-schedule/:id',
    component: EditWeekScheduleComponent,
  },
  {
    path: 'edit-schedule-override/:id',
    component: EditDayScheduleOverrideComponent,
  }   
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})

export class AdministrationRoutingModule { }
