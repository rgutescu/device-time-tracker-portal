import { Component,  OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DailyInterval } from '../models/daily-interval';
import { Schedule } from '../models/schedule';
import { getWeekDay } from '../../core/helpers/date-utils';
import { DailyIntervalForms } from '../models/daily-interval-forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AppState } from 'src/app/state/models/app-state';
import { Store } from '@ngrx/store';
import { MaterialService } from 'src/app/shared/material.service';
import { ServerOperationStatus } from 'src/app/state/models/server-operation-status.enum';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

import * as scheduleActions from '../../state/actions/schedule.actions';

@Component({
  selector: 'trk-edit-week-schedule',
  templateUrl: './edit-week-schedule.component.html',
  styleUrls: ['./edit-week-schedule.component.scss']
})
export class EditWeekScheduleComponent implements OnInit, OnDestroy {

  scheduleForm: FormGroup;

  scheduleData?: Schedule;  
  
  isProcessing: boolean = true;
  isDataLoaded: boolean = false;

  private isActionPerformed = false;

  private savingSubscription: Subscription;
  private loadingSubscription: Subscription;
  private paramsSubscription: Subscription;  
  isLargeResolution: boolean;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,    
    private materialService: MaterialService,
    public breakpointObserver: BreakpointObserver
    ) { }

  ngOnInit(): void {    
    this.breakpointObserver
      .observe(['(min-width: 800px)'])
      .subscribe((state: BreakpointState) => {
        this.isLargeResolution = state.matches;
    });
    
    this.paramsSubscription = this.route.paramMap
    .subscribe((params: ParamMap) => this.loadScheduleData(params.get('id')));

    this.savingSubscription = this.store
      .select(state => state.scheduleData.severOperationtStatus)
      .subscribe(status => this.toggleSavingState(status)); 
  }

  ngOnDestroy(): void {
    this.savingSubscription?.unsubscribe();
    this.loadingSubscription?.unsubscribe();
    this.paramsSubscription?.unsubscribe();
  }

  getFriendlyDayOfWeek(dayOfWeek: number): string {
    return getWeekDay(dayOfWeek);
  }

  saveScheduleClick() {
    this.isActionPerformed = true;

    const newScheduleData = this.fillScheduleDataFromFormElements();
    
    this.store.dispatch(new scheduleActions.EditUserWeeklyScheduleAction(newScheduleData));
    this.isProcessing = true;
  }

  cancelEditClicked() {
    this.redirectToMain();
  }

  private loadScheduleData(id: string): void {
    if (id) {
      this.store.dispatch(new scheduleActions.GetUserWeeklyScheduleAction(id));

      this.loadingSubscription = this.store
        .select(state => state.scheduleData.schedule)
        .subscribe(schedule => {
          if (schedule != null) {
            this.scheduleData = schedule;
            this.fillFormGroupData();
            this.isDataLoaded = true;
          }
        });
    } else {
      this.redirectToMain();
    }
  }

  private toggleSavingState(operationStatus: ServerOperationStatus) {
    this.isProcessing = (operationStatus === ServerOperationStatus.InProgress);

    if (!this.isActionPerformed) {
      return;
    }

    switch (operationStatus) {
      case ServerOperationStatus.Success: {
        this.redirectToMain();
        this.materialService.showSnackBar("Saving successfull!");
        break;
      }
      case ServerOperationStatus.Fail: {
        this.materialService.showSnackBar("Saving failed, please retry!");
        break;
      }
    }
  }

  private redirectToMain(): void {
    this.router.navigate(['administration']);
  }

  private fillFormGroupData(): void {
    const group: any = {};

    const dailyIntervalForms: DailyIntervalForms[] = []
    this.scheduleData.intervals.forEach((interval: DailyInterval, index: number) => {
      const formsInterval = Object.assign({}, interval) as DailyIntervalForms;
      formsInterval.startKey = `day${index}Start`,
      formsInterval.endKey = `day${index}End`,
      formsInterval.allowedMinutesKey = `day${index}AllowedMinutes`,
      dailyIntervalForms.push(formsInterval);

      group[formsInterval.startKey] = new FormControl(interval.startTime);
      group[formsInterval.endKey] = new FormControl(interval.endTime);
      group[formsInterval.allowedMinutesKey] = new FormControl(interval.allowedMinutes);
    });

    this.scheduleData = { ...this.scheduleData, intervals: dailyIntervalForms };

    this.scheduleForm = new FormGroup(group);
  }

  private fillScheduleDataFromFormElements(): Schedule {
    const newDailyIntervals: DailyInterval[] = [];

    this.scheduleData.intervals.forEach((interval: DailyIntervalForms) => {
      newDailyIntervals.push({
        day: interval.day,
        startTime: this.scheduleForm.value[interval.startKey],
        endTime: this.scheduleForm.value[interval.endKey],
        allowedMinutes: parseInt(this.scheduleForm.value[interval.allowedMinutesKey], 10),
      })
    });
    
    return {
      id: this.scheduleData.id,
      userId: this.scheduleData.userId,
      intervals: newDailyIntervals
    }
  }
}
