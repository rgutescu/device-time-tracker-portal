import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Schedule } from '../models/schedule';
import { getWeekDay } from '../../core/helpers/date-utils';

@Component({
  selector: 'trk-week-schedule',
  templateUrl: './week-schedule.component.html',
  styleUrls: ['./week-schedule.component.scss']
})
export class WeekScheduleComponent implements OnInit {

  @Input() schedule: Schedule;
  @Output() editSchedule = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit(): void {
  }

  editScheduleClicked(): void {
    this.editSchedule.emit(this.schedule.userId);
  }

  getFriendlyDayOfWeek(dayOfWeek: number): string {
    return getWeekDay(dayOfWeek);
  }
}
