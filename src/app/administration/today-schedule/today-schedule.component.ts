import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ScheduleOverride } from '../models/schedule-override';

@Component({
  selector: 'trk-today-schedule',
  templateUrl: './today-schedule.component.html',
  styleUrls: ['./today-schedule.component.scss']
})
export class TodayScheduleComponent implements OnInit {

  @Input() scheduleOverride: ScheduleOverride;
  @Input() userId: string;
  @Output() editScheduleOverride = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  editScheduleOverrideClicked(): void {
    this.editScheduleOverride.emit(this.userId);
  }
}