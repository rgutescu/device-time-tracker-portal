import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { UserWorkstation } from 'src/app/activity/models/user-workstation';
import { MaterialService } from 'src/app/shared/material.service';
import { AppState } from 'src/app/state/models/app-state';
import { getWeekDay } from '../../core/helpers/date-utils';
import { FormControl, FormGroup } from '@angular/forms';

import * as userActions from '../../state/actions/user.actions';
import { ServerOperationStatus } from 'src/app/state/models/server-operation-status.enum';

@Component({
  selector: 'trk-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit, OnDestroy {

  userData?: UserWorkstation;  
  
  isProcessing: boolean = true;
  isEditMode: boolean;  

  userForm: FormGroup;

  private isActionPerformed = false;

  private savingSubscription: Subscription;
  private loadingSubscription: Subscription;
  private paramsSubscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,    
    private materialService: MaterialService
    ) { }

  ngOnInit(): void {
    this.paramsSubscription = this.route.paramMap
      .subscribe((params: ParamMap) => this.fillUserData(params.get('id')));

    this.savingSubscription = this.store
        .select(state => state.userData.severOperationtStatus)
        .subscribe(status => this.toggleSavingState(status));    

    this.userForm = new FormGroup({
      userName: new FormControl(this.userData?.userName),
      workstation: new FormControl(this.userData?.workstation),
      active: new FormControl(this.userData?.active)
    });
  }

  ngOnDestroy() {
    this.savingSubscription?.unsubscribe();
    this.paramsSubscription?.unsubscribe();
    this.loadingSubscription?.unsubscribe();
  }

  getFriendlyDayOfWeek(dayOfWeek: number): string {
    return getWeekDay(dayOfWeek);
  }

  saveUserClicked() {
    this.isActionPerformed = true;

    const newUserData: UserWorkstation = {
      id: this.userData.id,
      userName: this.userForm.value.userName,
      workstation: this.userForm.value.workstation,
      active: this.userForm.value.active,
    };

    if(this.isEditMode) {
      this.store.dispatch(new userActions.EditUserWorkstationsScheduleAction(newUserData));
    }
    else {
      this.store.dispatch(new userActions.AddUserWorkstationsScheduleAction(newUserData));
    }

    this.isProcessing = true;
  }

  cancelEditClicked() {
    this.redirectToMain();
  }

  private redirectToMain(): void {
    this.router.navigate(['administration']);
  }

  private fillUserData(id: string): void {
    if (id) {
      this.isEditMode = true;
      this.store.dispatch(new userActions.GetSingleUserWorkstationsAction(id));

      this.loadingSubscription = this.store
        .select(state => state.userData.selectedUser)
        .subscribe(userWorkstation => this.userData = userWorkstation);
    } else {
      this.fillUserDataDefaults();
      this.isEditMode = false;
      this.isProcessing = false;
    }
  }

  private fillUserDataDefaults(): void {
   this.userData = {
      id: '',
      userName: '',
      workstation: '',
      active: true
    };
  }
  
  private toggleSavingState(operationStatus: ServerOperationStatus) {
    this.isProcessing = (operationStatus === ServerOperationStatus.InProgress);

    if (!this.isActionPerformed) {
      return;
    }

    switch (operationStatus) {
      case ServerOperationStatus.Success: {
        this.redirectToMain();
        this.materialService.showSnackBar("Saving successfull!");
        break;
      }
      case ServerOperationStatus.Fail: {
        this.materialService.showSnackBar("Saving failed, please retry!");
        break;
      }
    }
  }  
}
