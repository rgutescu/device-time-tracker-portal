import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { EventEmitter } from '@angular/core';
import { UserWorkstation } from 'src/app/activity/models/user-workstation';
import { AppState } from 'src/app/state/models/app-state';

import * as userActions from '../../state/actions/user.actions';

@Component({
  selector: 'trk-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  @Input() userWorkstations: UserWorkstation[];
  @Output() selectUser = new EventEmitter<UserWorkstation>();
  @Output() addUser = new EventEmitter();
  @Output() editUser = new EventEmitter<string>();

  displayedColumns: string[] = ['userName', 'workstation', 'active'];
  dataSource: MatTableDataSource<UserWorkstation>;
  selectedUser: UserWorkstation;

  @ViewChild(MatSort) sort: MatSort;
  
  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.dataBind();

    if (this.userWorkstations && this.userWorkstations.length) {
      this.selectUserClicked(this.userWorkstations[0]);
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  selectUserClicked(user: UserWorkstation) {
    if (this.selectedUser !== user) {
      this.selectedUser = user;
      this.store.dispatch(new userActions.SelectUserWorkstationsAction(user));
    }
  }

  addUserClicked(): void {
    this.addUser.emit(null);
  }

  editUserClicked(): void {
    this.editUser.emit(this.selectedUser.id);
  }

  private dataBind(): void {
    this.dataSource = new MatTableDataSource(this.userWorkstations);
  }
}
