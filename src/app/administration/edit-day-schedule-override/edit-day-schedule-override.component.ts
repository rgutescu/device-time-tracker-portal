import { Component, OnDestroy, OnInit } from '@angular/core';
import { ScheduleOverride } from '../models/schedule-override';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { MaterialService } from 'src/app/shared/material.service';
import { AppState } from 'src/app/state/models/app-state';
import { getTodayDateWithoutTime, getWeekDay } from '../../core/helpers/date-utils';
import { FormControl, FormGroup } from '@angular/forms';

import * as scheduleActions from '../../state/actions/schedule.actions';
import { ServerOperationStatus } from 'src/app/state/models/server-operation-status.enum';
import { ScheduleOverrideCriteria } from '../models/schedule-override-criteria';
import { DailyInterval } from '../models/daily-interval';

@Component({
  selector: 'trk-edit-day-schedule-override',
  templateUrl: './edit-day-schedule-override.component.html',
  styleUrls: ['./edit-day-schedule-override.component.scss'],
})
export class EditDayScheduleOverrideComponent implements OnInit, OnDestroy {

  scheduleOverrideData?: ScheduleOverride;  
  
  isProcessing: boolean = true;
  isEditMode: boolean;  
  isDataLoaded: boolean = false;

  scheduleOverrideForm: FormGroup;

  private isActionPerformed = false;
  private userId: string;
  private savingSubscription: Subscription;
  private loadingSubscription: Subscription;
  private paramsSubscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,    
    private materialService: MaterialService
    ) { }

  ngOnInit(): void {
    this.paramsSubscription = this.route.paramMap
    .subscribe((params: ParamMap) => this.loadScheduleOverrideData(params.get('id')));

    this.savingSubscription = this.store
      .select(state => state.scheduleData.severOperationtStatus)
      .subscribe(status => this.toggleSavingState(status)); 
  }

  ngOnDestroy() {
    this.savingSubscription?.unsubscribe();
    this.paramsSubscription?.unsubscribe();
    this.loadingSubscription?.unsubscribe();
  }

  getFriendlyDayOfWeek(dayOfWeek: number): string {
    return getWeekDay(dayOfWeek);
  }

  saveScheduleOverrideClicked() {
    this.isActionPerformed = true;

    const newScheduleOverrideData = this.fillScheduleOverrideDataFromFormElements();
    
    this.store.dispatch(new scheduleActions.EditUserOverrideScheduleAction(newScheduleOverrideData));
    this.isProcessing = true;
  }  

  cancelEditClicked() {
    this.redirectToMain();
  }

  private redirectToMain(): void {
    this.router.navigate(['administration']);
  }

  private getDefaultScheduleOverrideData(): ScheduleOverride {
    return {
      id: '',
      userId: this.userId,
      date: getTodayDateWithoutTime(),
      interval: {
        startTime: '08:00',
        endTime: '21:00',
        allowedMinutes: 0
      }
    }
  }

  private loadScheduleOverrideData(userId: string): void {
    this.userId = userId;
    const scheduleOverrideCritiera: ScheduleOverrideCriteria = {
      userId: userId,
      date: getTodayDateWithoutTime().toISOString()
    };
    if (userId) {
      this.store.dispatch(new scheduleActions.GetUserOverrideScheduleAction(scheduleOverrideCritiera));

      this.loadingSubscription = this.store
        .select(state => state.scheduleData)
        .subscribe(scheduleData => {
          
          if (scheduleData.severOperationtStatus !== ServerOperationStatus.InProgress) {
            this.scheduleOverrideData = scheduleData.scheduleOverride || this.getDefaultScheduleOverrideData();
            this.fillFormGroupData();
            this.isDataLoaded = true;
          }
        });
    } else {
      this.redirectToMain();
    }
  }  
 
  private toggleSavingState(operationStatus: ServerOperationStatus) {
    this.isProcessing = (operationStatus === ServerOperationStatus.InProgress);

    if (!this.isActionPerformed) {
      return;
    }

    switch (operationStatus) {
      case ServerOperationStatus.Success: {
        this.redirectToMain();
        this.materialService.showSnackBar("Saving successfull!");
        break;
      }
      case ServerOperationStatus.Fail: {
        this.materialService.showSnackBar("Saving failed, please retry!");
        break;
      }
    }
  }  
 
  private fillFormGroupData(): void {
    this.scheduleOverrideForm = new FormGroup({
      date: new FormControl(this.scheduleOverrideData?.date),
      startTime: new FormControl(this.scheduleOverrideData?.interval.startTime),
      endTime: new FormControl(this.scheduleOverrideData?.interval.endTime),
      allowedMinutes: new FormControl(this.scheduleOverrideData?.interval.allowedMinutes)
    });
  }

  private fillScheduleOverrideDataFromFormElements(): ScheduleOverride {
    const newDailyInterval: DailyInterval = {
      startTime: this.scheduleOverrideForm.value.startTime,
      endTime: this.scheduleOverrideForm.value.endTime,
      allowedMinutes: parseInt(this.scheduleOverrideForm.value.allowedMinutes, 10)
    }
   
    return {
      id: this.scheduleOverrideData.id,
      userId: this.scheduleOverrideData.userId,
      date: this.scheduleOverrideForm.value.date,
      interval: newDailyInterval
    }
  }
}
