import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDayScheduleOverrideComponent } from './edit-day-schedule-override.component';

describe('EditWeekScheduleOverrideComponent', () => {
  let component: EditDayScheduleOverrideComponent;
  let fixture: ComponentFixture<EditDayScheduleOverrideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditDayScheduleOverrideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDayScheduleOverrideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
