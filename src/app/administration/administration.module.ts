import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationMainComponent } from './administration-main/administration-main.component';
import { AdministrationRoutingModule } from './administration-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { WeekScheduleComponent } from './week-schedule/week-schedule.component';
import { TodayScheduleComponent } from './today-schedule/today-schedule.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EditWeekScheduleComponent } from './edit-week-schedule/edit-week-schedule.component';
import { EditDayScheduleOverrideComponent } from './edit-day-schedule-override/edit-day-schedule-override.component';

@NgModule({
  declarations: [
    AdministrationMainComponent, 
    UserListComponent, 
    WeekScheduleComponent, 
    TodayScheduleComponent, 
    EditUserComponent, 
    EditWeekScheduleComponent, 
    EditDayScheduleOverrideComponent
  ],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [
    AdministrationMainComponent
  ]
})
export class AdministrationModule { }
