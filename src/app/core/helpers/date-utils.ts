export function getWeekDay(dayOfWeek: number): string {
    var weekdays = new Array(
        "None", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
    );
    return weekdays[dayOfWeek];
}

export function getTodayDateWithoutTime(): Date {
    const today = new Date();
    return new Date(`${today.getFullYear()}-${1 + today.getMonth()}-${today.getDate()}`); 
}

export function getTomorrowDateWithoutTime() : Date {
    return new Date(getTodayDateWithoutTime().getTime() + 86400000); // tomorrow
}