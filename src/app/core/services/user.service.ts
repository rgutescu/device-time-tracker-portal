import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserWorkstation } from '../../activity/models/user-workstation';
import { AppConfigService } from './app-config.service';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseApiService<UserWorkstation> {

  constructor(http: HttpClient, configService: AppConfigService) {
    super(http, configService);
  }

  getUserWorkstations() : Observable<UserWorkstation[]> {
    return super.getMany('user');
  }

  getUserWorkstationById(id: string) : Observable<UserWorkstation> {
    return super.get('user', id);
  }

  addUserWorkstation(userWorkstation: UserWorkstation) : Observable<UserWorkstation> {
    return super.post('user', userWorkstation);
  }

  editUserWorkstation(userWorkstation: UserWorkstation) : Observable<UserWorkstation> {
    return super.put(`user/${userWorkstation.id}`, userWorkstation);
  }
}
