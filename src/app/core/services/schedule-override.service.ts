import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ScheduleOverride } from '../../administration/models/schedule-override';
import { ScheduleOverrideCriteria } from '../../administration/models/schedule-override-criteria';
import { AppConfigService } from './app-config.service';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class ScheduleOverrideService extends BaseApiService<ScheduleOverride>{

  constructor(http: HttpClient, configService: AppConfigService) {
    super(http, configService);
  }

  getUserOverrideSchedule(scheduleOverrideCriteria: ScheduleOverrideCriteria): Observable<ScheduleOverride> {
    return super.get(`user/${scheduleOverrideCriteria.userId}/scheduleOverride?date=${scheduleOverrideCriteria.date}`, null);
  }

  editUserOverrideSchedule(scheduleOverride: ScheduleOverride) : Observable<ScheduleOverride> {
    return super.put(`user/${scheduleOverride.userId}/scheduleOverride`, scheduleOverride);
  }
}
