import { TestBed } from '@angular/core/testing';

import { ScheduleOverrideService } from './schedule-override.service';

describe('ScheduleOverrideService', () => {
  let service: ScheduleOverrideService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduleOverrideService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
