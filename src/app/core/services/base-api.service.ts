import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { KeyValuePair } from '../models/tuple';
import { AppConfigService } from './app-config.service';

export abstract class BaseApiService<T> {

  constructor(
    private http: HttpClient,
    private configService: AppConfigService) { 
    }

  protected getMany(pathUrl: string, params: KeyValuePair<string, string>[] = null) : Observable<T[]> {
    let getManyUrl = `${this.configService.apiBaseUrl}/${pathUrl}`;

    if (params != null && params.length > 0 ) {
      getManyUrl += "?";
      params.forEach(element => {
        getManyUrl += `${element.key}=${element.value}&`;
      });
    }

    return this.http.get<T[]>(getManyUrl);
  }

  protected get(pathUrl: string, id: string) : Observable<T> {
    let getUrl = `${this.configService.apiBaseUrl}/${pathUrl}`;
    if (id != null) {
      getUrl += `/${id}`;
    }

    return this.http.get<T>(getUrl);
  }

  protected post(pathUrl: string, entity: T) : Observable<T> {
    return this.http.post<T>(`${this.configService.apiBaseUrl}/${pathUrl}`, entity);
  }

  protected put(pathUrl: string, entity: T) : Observable<T> {
    return this.http.put<T>(`${this.configService.apiBaseUrl}/${pathUrl}`, entity);
  }

  protected delete(pathUrl: string, id: string) : Observable<T> {
    return this.http.delete<T>(`${this.configService.apiBaseUrl}/${pathUrl}/${id}`);
  }
}
