import { Injectable } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { Store } from '@ngrx/store';
import { debounce, debounceTime, filter } from 'rxjs/operators';
import { AppState } from 'src/app/state/models/app-state';
import * as layoutActions from '../../state/actions/layout.actions';

@Injectable({
  providedIn: 'root',
})
export class RoutingService {

  constructor(
    private router: Router,
    private store: Store<AppState>,) {
    router.events.pipe(
      filter((e) => e instanceof RouterEvent),
      debounceTime(250)
    )
    .subscribe((e) => {
      const ev = e as RouterEvent;
      this.store.dispatch(new layoutActions.UpdateNavigationMessageAction(this.mapNavigationMessageFromRouteUrl(ev.url)));
    });
    }

  // TODO: refactor at some point :) and introduce constants
  private mapNavigationMessageFromRouteUrl(url: string): string {
    if (url.indexOf('activity') >= 0) {
      return 'Activity Log';
    }
    if (url.indexOf('dashboard') >= 0) {
      return 'Dashboard';
    }
    if (url.indexOf('add-user') >= 0) {
      return 'Administration - Add user';
    }
    if (url.indexOf('edit-user') >= 0) {
      return 'Administration - Edit user';
    }
    if (url.indexOf('edit-schedule-override') >= 0) {
      return 'Administration - Override schedule';
    }
    if (url.indexOf('edit-schedule') >= 0) {
      return 'Administration - Edit schedule';
    }
    if (url.indexOf('administration') >= 0) {
      return 'Administration';
    }
    if (url.indexOf('login') >= 0) {
      return 'Login';
    }

    return 'To be determined';
  }
}
