import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from '../models/app-settings';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  private appSettings: AppSettings;

  constructor(private http: HttpClient) { }

  loadAppConfig() {
    return this.http.get('/assets/config.json')
      .toPromise()
      .then(data => {
        this.appSettings = <AppSettings>data;
      });
  }

  get apiBaseUrl() {

    if (!this.appSettings) {
      throw Error('Config file not loaded!');
    }

    return this.appSettings.apiBaseUrl;
  }
}
