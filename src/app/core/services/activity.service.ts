import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivityDetails } from '../../activity/models/activity-details';
import { ActivitySearchCriteria } from '../../activity/models/activity-search-criteria';
import { KeyValuePair } from '../models/tuple';
import { AppConfigService } from './app-config.service';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class ActivityService extends BaseApiService<ActivityDetails> {

  constructor(http: HttpClient, configService: AppConfigService) {
    super(http, configService);
   }

   searchActivities(searchCriteria: ActivitySearchCriteria) : Observable<ActivityDetails[]> {
    const params: KeyValuePair<string, string>[] = [];
    params.push(new KeyValuePair<string, string>("userId", searchCriteria.userId));
    params.push(new KeyValuePair<string, string>("start", searchCriteria.startDate));
    params.push(new KeyValuePair<string, string>("end", searchCriteria.endDate));

    return super.getMany('activityLog', params);
   }
}
