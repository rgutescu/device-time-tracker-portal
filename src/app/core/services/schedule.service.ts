import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Schedule } from '../../administration/models/schedule';
import { AppConfigService } from './app-config.service';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService extends BaseApiService<Schedule>{

  constructor(http: HttpClient, configService: AppConfigService) {
    super(http, configService);
  }

  getUserWeeklySchedule(userId: string): Observable<Schedule> {
    return super.get(`user/${userId}/schedule`, null);
  }

  editUserWeeklySchedule(schedule: Schedule) : Observable<Schedule> {
    return super.put(`user/${schedule.userId}/schedule/`, schedule);
  }
}
