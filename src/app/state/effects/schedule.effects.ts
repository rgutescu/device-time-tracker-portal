import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { ScheduleOverrideService } from 'src/app/core/services/schedule-override.service';
import { ScheduleService } from 'src/app/core/services/schedule.service';

import * as scheduleActions from '../actions/schedule.actions';

@Injectable()
export class ScheduleEffects {
    constructor(
      private actions$: Actions,
      private scheduleService: ScheduleService,
      private scheduleOverrideService: ScheduleOverrideService) { }

    @Effect() getUserWeeklySchedule = this.actions$.pipe
        (
        ofType(scheduleActions.GET_USER_WEEKLY_SCHEDULE),
        switchMap((action) => 
          this.scheduleService.getUserWeeklySchedule((action as scheduleActions.GetUserWeeklyScheduleAction).payload)
          .pipe(
            map(scheduleList => new scheduleActions.GetUserWeeklyScheduleSuccessAction(scheduleList)),
            catchError(error => of(new scheduleActions.GetUserWeeklyScheduleFailedAction({details: error.details, message: error.message})))
          )
    ));

    @Effect() getUserOverrideSchedule = this.actions$.pipe
        (
        ofType(scheduleActions.GET_USER_OVERRIDE_SCHEDULE),
        switchMap((action) => 
          this.scheduleOverrideService.getUserOverrideSchedule((action as scheduleActions.GetUserOverrideScheduleAction).payload)
          .pipe(
            map(schedule => new scheduleActions.GetUserOverrideScheduleSuccessAction(schedule)),
            catchError(error => of(new scheduleActions.GetUserOverrideScheduleFailedAction({details: error.details, message: error.message})))
          )
    ));
    
    @Effect() editUserWeeklySchedule = this.actions$.pipe
        (
        ofType(scheduleActions.EDIT_USER_WEEKLY_SCHEDULE),
        switchMap((action) => 
          this.scheduleService.editUserWeeklySchedule((action as scheduleActions.EditUserWeeklyScheduleAction).payload)
          .pipe(
            map((schedule) => new scheduleActions.EditUserWeeklyScheduleSuccessAction(schedule)),
            catchError(error => of(new scheduleActions.EditUserWeeklyScheduleFailedAction({details: error.details, message: error.message})))
          )
    ));

    @Effect() editUserOverrideSchedule = this.actions$.pipe
        (
        ofType(scheduleActions.EDIT_USER_OVERRIDE_SCHEDULE),
        switchMap((action) => 
          this.scheduleOverrideService.editUserOverrideSchedule((action as scheduleActions.EditUserOverrideScheduleAction).payload)
          .pipe(
            map((scheduleOverride) => new scheduleActions.EditUserOverrideScheduleSuccessAction(scheduleOverride)),
            catchError(error => of(new scheduleActions.EditUserOverrideScheduleFailedAction({details: error.details, message: error.message})))
          )
    ));    
}
