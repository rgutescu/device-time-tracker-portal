import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, switchMap, map, delay, tap } from 'rxjs/operators';
import { UserService } from 'src/app/core/services/user.service';

import * as userActions from '../actions/user.actions';

@Injectable()
export class UserEffects {
    constructor(private actions$: Actions, private userService: UserService) { }

    @Effect() loadUserWorkstations = this.actions$.pipe
        (
        ofType(userActions.GET_USERWORKSTATIONS),
        switchMap(() => 
          this.userService.getUserWorkstations()
          .pipe(
            map(userWorkstations => new userActions.GetUserWorkstationsSuccessAction(userWorkstations)),
            catchError(error => of(new userActions.GetUserWorkstationsFailedAction({details: error.details, message: error.message})))
          )
        ));

      @Effect() getSingleUserWorkstation = this.actions$.pipe
        (
        ofType(userActions.GET_SINGLE_USERWORKSTATION),
        switchMap((action) => 
          this.userService.getUserWorkstationById((action as userActions.GetSingleUserWorkstationsAction).payload)
          .pipe(
            map(userWorkstation => new userActions.GetSingleUserWorkstationsSuccessAction(userWorkstation)),
            catchError(error => of(new userActions.GetSingleUserWorkstationsFailedAction({details: error.details, message: error.message})))
          )
        ));        

    @Effect() addUserWorkstation = this.actions$.pipe
        (
        ofType(userActions.ADD_USERWORKSTATION),
        switchMap((action) => 
          this.userService.addUserWorkstation((action as userActions.AddUserWorkstationsScheduleAction).payload)
          .pipe(
            delay(2000),
            map(() => new userActions.AddUserWorkstationsScheduleSuccessAction()),
            catchError(error => of(new userActions.AddUserWorkstationsScheduleFailedAction({details: error.details, message: error.message})))
          )
        )) 
        
    @Effect() editUserWorkstation = this.actions$.pipe
        (
        ofType(userActions.EDIT_USERWORKSTATION),
        switchMap((action) => 
          this.userService.editUserWorkstation((action as userActions.EditUserWorkstationsScheduleAction).payload)
          .pipe(
            delay(2000),
            map((userWorkstation) => new userActions.EditUserWorkstationsScheduleSuccessAction(userWorkstation)),
            catchError(error => of(new userActions.EditUserWorkstationsScheduleFailedAction({details: error.details, message: error.message})))
          )
        ))           
}
