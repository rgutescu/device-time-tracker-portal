import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { ActivityService } from 'src/app/core/services/activity.service';

import * as activityActions from '../actions/activity.actions';

@Injectable()
export class ActivityEffects {
    constructor(private actions$: Actions, private activityService: ActivityService ) { }

    @Effect() searchActivities = this.actions$.pipe
        (
        ofType(activityActions.SEARCH_ACTIVITIES),
        switchMap(action => 
          this.activityService.searchActivities((action as activityActions.SearchActivitiesAction).payload)
          .pipe(
            map(activityActionsList => new activityActions.SearchActivitiesSuccessAction(activityActionsList)),
            catchError(error => of(new activityActions.SearchActivitiesFailedAction({details: error.details, message: error.message})))
          )
        ));
}
