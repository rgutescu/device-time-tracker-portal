export interface ErrorDetails {
    message: string;
    details: string;
}
