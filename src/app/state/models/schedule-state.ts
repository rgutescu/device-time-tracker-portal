import { Schedule } from "src/app/administration/models/schedule";
import { ScheduleOverride } from "src/app/administration/models/schedule-override";
import { BaseState } from "./base-state";

export interface ScheduleState extends BaseState<void>{
    schedule?: Schedule;
    scheduleOverride?: ScheduleOverride;
}
