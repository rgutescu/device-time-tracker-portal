import { ActivityState } from './activity-state';
import { LayoutState } from './layout-state';
import { ScheduleState } from './schedule-state';
import { UserState } from './user-state';

export interface AppState {
    activityData: ActivityState;
    userData: UserState;
    scheduleData: ScheduleState;
    layoutData: LayoutState;
}
