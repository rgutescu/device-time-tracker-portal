import { ActivityDetails } from 'src/app/activity/models/activity-details';
import { BaseState } from './base-state';

export interface ActivityState extends BaseState<ActivityDetails> {

}
