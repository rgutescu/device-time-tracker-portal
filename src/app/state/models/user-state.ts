import { UserWorkstation } from 'src/app/activity/models/user-workstation';
import { BaseState } from './base-state';

export interface UserState extends BaseState<UserWorkstation> {
    selectedUser?: UserWorkstation;

}
