export enum ServerOperationStatus {
    None = 0,
    InProgress = 1,
    Success = 2,
    Fail = 3
}
