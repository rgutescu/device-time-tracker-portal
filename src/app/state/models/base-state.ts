import { ErrorDetails } from './error-details';
import { ServerOperationStatus } from './server-operation-status.enum';

export interface BaseState<T> {
    items?: Array<T>;
    errorDetails?: ErrorDetails;
    severOperationtStatus: ServerOperationStatus;
}
