import { NavLink } from "src/app/shared/models/nav-link";

export interface LayoutState {
    mainNavBarLinks: NavLink[];
    navigationMessage: string;
}
