import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { activityReducer } from './reducers/activity.reducers';
import { userReducer } from './reducers/user.reducers';
import { scheduleReducer } from './reducers/schedule.reducers';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './effects/user.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ActivityEffects } from './effects/activity.effects';
import { ScheduleEffects } from './effects/schedule.effects';
import { layoutReducer } from './reducers/layout.reducers';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // StoreModule.forRoot({}),
    // StoreModule.forFeature('activity', [activityReducer, userReducer]),
    // StoreModule.forRoot([activityReducer, userReducer]),
    StoreModule.forRoot({ activityData: activityReducer, userData: userReducer, scheduleData: scheduleReducer, layoutData: layoutReducer }),
    // activities
    // EffectsModule.forRoot([]),
    // EffectsModule.forFeature([UserEffects]),
    EffectsModule.forRoot([ActivityEffects, UserEffects, ScheduleEffects]),
    StoreDevtoolsModule.instrument()
  ]
})
export class StateModule { }
