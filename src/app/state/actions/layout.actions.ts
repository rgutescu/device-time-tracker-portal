import { Action } from '@ngrx/store';

export const UPDATE_NAVIGATION_INFO = 'UPDATE_NAVIGATION_INFO';

export class UpdateNavigationMessageAction implements Action {
    readonly type = UPDATE_NAVIGATION_INFO;
    constructor(public payload: string) { }
}

export type LayoutActionType =
    UpdateNavigationMessageAction;    
