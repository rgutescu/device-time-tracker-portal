import { Action } from '@ngrx/store';
import { ActivityDetails } from 'src/app/activity/models/activity-details';
import { ActivitySearchCriteria } from 'src/app/activity/models/activity-search-criteria';
import { ErrorDetails } from '../models/error-details';

export const SEARCH_ACTIVITIES = 'SEARCH_ACTIVITIES';
export const SEARCH_ACTIVITIES_SUCCESS = 'SEARCH_ACTIVITIES_SUCCESS';
export const SEARCH_ACTIVITIES_FAILED = 'SEARCH_ACTIVITIES_FAILED';

export class SearchActivitiesAction implements Action {
    readonly type = SEARCH_ACTIVITIES;
    constructor(public payload: ActivitySearchCriteria) { }
}

export class SearchActivitiesSuccessAction implements Action {
    readonly type = SEARCH_ACTIVITIES_SUCCESS;
    constructor(public payload: ActivityDetails[]) { }
}

export class SearchActivitiesFailedAction implements Action {
    readonly type = SEARCH_ACTIVITIES_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export type SearchActivitiesActionType =
    SearchActivitiesAction |
    SearchActivitiesSuccessAction |
    SearchActivitiesFailedAction;
