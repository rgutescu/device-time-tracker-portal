import { Action } from '@ngrx/store';
import { Schedule } from 'src/app/administration/models/schedule';
import { ScheduleOverride } from 'src/app/administration/models/schedule-override';
import { ScheduleOverrideCriteria } from 'src/app/administration/models/schedule-override-criteria';
import { ErrorDetails } from '../models/error-details';

export const GET_USER_WEEKLY_SCHEDULE = 'GET_USER_WEEKLY_SCHEDULE';
export const GET_USER_WEEKLY_SCHEDULE_SUCCESS = 'GET_USER_WEEKLY_SCHEDULE_SUCCESS';
export const GET_USER_WEEKLY_SCHEDULE_FAILED = 'GET_USER_WEEKLY_SCHEDULE_FAILED';

export const GET_USER_OVERRIDE_SCHEDULE = 'GET_USER_OVERRIDE_SCHEDULE';
export const GET_USER_OVERRIDE_SCHEDULE_SUCCESS = 'GET_USER_OVERRIDE_SCHEDULE_SUCCESS';
export const GET_USER_OVERRIDE_SCHEDULE_FAILED = 'GET_USER_OVERRIDE_SCHEDULE_FAILED';

export const EDIT_USER_WEEKLY_SCHEDULE = 'EDIT_USER_WEEKLY_SCHEDULE';
export const EDIT_USER_WEEKLY_SCHEDULE_SUCCESS = 'EDIT_USER_WEEKLY_SCHEDULE_SUCCESS';
export const EDIT_USER_WEEKLY_SCHEDULE_FAILED = 'EDIT_USER_WEEKLY_SCHEDULE_FAILED';

export const EDIT_USER_OVERRIDE_SCHEDULE = 'EDIT_USER_OVERRIDE_SCHEDULE';
export const EDIT_USER_OVERRIDE_SCHEDULE_SUCCESS = 'EDIT_USER_OVERRIDE_SCHEDULE_SUCCESS';
export const EDIT_USER_OVERRIDE_SCHEDULE_FAILED = 'EDIT_USER_OVERRIDE_SCHEDULE_FAILED';

export class GetUserWeeklyScheduleAction implements Action {
    readonly type = GET_USER_WEEKLY_SCHEDULE;
    constructor(public payload: string) { }
}

export class GetUserWeeklyScheduleSuccessAction implements Action {
    readonly type = GET_USER_WEEKLY_SCHEDULE_SUCCESS;
    constructor(public payload: Schedule) { }
}

export class GetUserWeeklyScheduleFailedAction implements Action {
    readonly type = GET_USER_WEEKLY_SCHEDULE_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export class GetUserOverrideScheduleAction implements Action {
    readonly type = GET_USER_OVERRIDE_SCHEDULE;
    constructor(public payload: ScheduleOverrideCriteria) { }
}

export class GetUserOverrideScheduleSuccessAction implements Action {
    readonly type = GET_USER_OVERRIDE_SCHEDULE_SUCCESS;
    constructor(public payload: ScheduleOverride) { }
}

export class GetUserOverrideScheduleFailedAction implements Action {
    readonly type = GET_USER_OVERRIDE_SCHEDULE_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export class EditUserWeeklyScheduleAction implements Action {
    readonly type = EDIT_USER_WEEKLY_SCHEDULE;
    constructor(public payload: Schedule) { }
}

export class EditUserWeeklyScheduleSuccessAction implements Action {
    readonly type = EDIT_USER_WEEKLY_SCHEDULE_SUCCESS;
    constructor(public payload: Schedule) { }
}

export class EditUserWeeklyScheduleFailedAction implements Action {
    readonly type = EDIT_USER_WEEKLY_SCHEDULE_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export class EditUserOverrideScheduleAction implements Action {
    readonly type = EDIT_USER_OVERRIDE_SCHEDULE;
    constructor(public payload: ScheduleOverride) { }
}

export class EditUserOverrideScheduleSuccessAction implements Action {
    readonly type = EDIT_USER_OVERRIDE_SCHEDULE_SUCCESS;
    constructor(public payload: ScheduleOverride) { }
}

export class EditUserOverrideScheduleFailedAction implements Action {
    readonly type = EDIT_USER_OVERRIDE_SCHEDULE_FAILED;
    constructor(public payload: ErrorDetails) { }    
}

export type ScheduleActionType =
    GetUserWeeklyScheduleAction |
    GetUserWeeklyScheduleSuccessAction |
    GetUserWeeklyScheduleFailedAction |
    GetUserOverrideScheduleAction |
    GetUserOverrideScheduleSuccessAction |
    GetUserOverrideScheduleFailedAction |
    EditUserWeeklyScheduleAction |
    EditUserWeeklyScheduleSuccessAction |
    EditUserWeeklyScheduleFailedAction |
    EditUserOverrideScheduleAction |
    EditUserOverrideScheduleSuccessAction |
    EditUserOverrideScheduleFailedAction;