import { Action } from '@ngrx/store';
import { UserWorkstation } from 'src/app/activity/models/user-workstation';
import { ErrorDetails } from '../models/error-details';

export const GET_USERWORKSTATIONS = 'GET_USERWORKSTATIONS';
export const GET_USERWORKSTATIONS_SUCCESS = 'GET_USERWORKSTATIONS_SUCCESS';
export const GET_USERWORKSTATIONS_FAILED = 'GET_USERWORKSTATIONS_FAILED';

export const GET_SINGLE_USERWORKSTATION = 'GET_SINGLE_USERWORKSTATION';
export const GET_SINGLE_USERWORKSTATION_SUCCESS = 'GET_SINGLE_USERWORKSTATION_SUCCESS';
export const GET_SINGLE_USERWORKSTATION_FAILED = 'GET_SINGLE_USERWORKSTATION_FAILED';

export const SELECT_USERWORKSTATIONS = 'SELECT_USERWORKSTATIONS';

export const ADD_USERWORKSTATION = 'ADD_USERWORKSTATION';
export const ADD_USERWORKSTATION_SUCCESS = 'ADD_USERWORKSTATION_SUCCESS';
export const ADD_USERWORKSTATION_FAILED = 'ADD_USERWORKSTATION_FAILED';

export const EDIT_USERWORKSTATION = 'EDIT_USERWORKSTATION';
export const EDIT_USERWORKSTATION_SUCCESS = 'EDIT_USERWORKSTATION_SUCCESS';
export const EDIT_USERWORKSTATION_FAILED = 'EDIT_USERWORKSTATION_FAILED';

export class GetUserWorkstationsAction implements Action {
    readonly type = GET_USERWORKSTATIONS;
    constructor() { }
}

export class GetUserWorkstationsSuccessAction implements Action {
    readonly type = GET_USERWORKSTATIONS_SUCCESS;
    constructor(public payload: UserWorkstation[]) { }
}

export class GetUserWorkstationsFailedAction implements Action {
    readonly type = GET_USERWORKSTATIONS_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export class GetSingleUserWorkstationsAction implements Action {
    readonly type = GET_SINGLE_USERWORKSTATION;
    constructor(public payload: string) { }
}

export class GetSingleUserWorkstationsSuccessAction implements Action {
    readonly type = GET_SINGLE_USERWORKSTATION_SUCCESS;
    constructor(public payload: UserWorkstation) { }
}

export class GetSingleUserWorkstationsFailedAction implements Action {
    readonly type = GET_SINGLE_USERWORKSTATION_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export class SelectUserWorkstationsAction implements Action {
    readonly type = SELECT_USERWORKSTATIONS;
    constructor(public payload: UserWorkstation) { }
}

export class AddUserWorkstationsScheduleAction implements Action {
    readonly type = ADD_USERWORKSTATION;
    constructor(public payload: UserWorkstation) { }
}

export class AddUserWorkstationsScheduleSuccessAction implements Action {
    readonly type = ADD_USERWORKSTATION_SUCCESS;
    constructor() { }
}

export class  AddUserWorkstationsScheduleFailedAction implements Action {
    readonly type = ADD_USERWORKSTATION_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export class EditUserWorkstationsScheduleAction implements Action {
    readonly type = EDIT_USERWORKSTATION;
    constructor(public payload: UserWorkstation) { }
}

export class EditUserWorkstationsScheduleSuccessAction implements Action {
    readonly type = EDIT_USERWORKSTATION_SUCCESS;
    constructor(public payload: UserWorkstation) { }
}

export class  EditUserWorkstationsScheduleFailedAction implements Action {
    readonly type = EDIT_USERWORKSTATION_FAILED;
    constructor(public payload: ErrorDetails) { }
}

export type UserActionType =
    GetUserWorkstationsAction |
    GetUserWorkstationsSuccessAction |
    GetUserWorkstationsFailedAction |
    GetSingleUserWorkstationsAction |
    GetSingleUserWorkstationsSuccessAction |
    GetSingleUserWorkstationsFailedAction |
    SelectUserWorkstationsAction |
    AddUserWorkstationsScheduleAction |
    AddUserWorkstationsScheduleSuccessAction |
    AddUserWorkstationsScheduleFailedAction |
    EditUserWorkstationsScheduleAction |
    EditUserWorkstationsScheduleSuccessAction |
    EditUserWorkstationsScheduleFailedAction;    
