import * as layoutActions from '../actions/layout.actions';
import { LayoutState } from '../models/layout-state';

const defaultLayoutState: LayoutState = {
    mainNavBarLinks: [
        { label: 'Dashboard', routeLink: '/dashboard', isActive: false},
        { label: 'Activity', routeLink: '/activity', isActive: true },
        { label: 'Administration', routeLink: '/administration', isActive: false }
      ],
      navigationMessage: 'Activity Data'
};

export function layoutReducer(
    state: LayoutState = defaultLayoutState,
    action: layoutActions.LayoutActionType): LayoutState {
    switch (action.type) {
        case layoutActions.UPDATE_NAVIGATION_INFO: {
            return {...state, navigationMessage: action.payload };
        }
        default: {
            return state;
        }
    }
}
