import * as userActions from '../actions/user.actions';
import { ServerOperationStatus } from '../models/server-operation-status.enum';
import { UserState } from '../models/user-state';

const defaultUserState: UserState = {
    severOperationtStatus: ServerOperationStatus.None
};

export function userReducer(
    state: UserState = defaultUserState,
    action: userActions.UserActionType): UserState {
    switch (action.type) {
        // GET_USERWORKSTATIONS
        case userActions.GET_USERWORKSTATIONS: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case userActions.GET_USERWORKSTATIONS_SUCCESS: {
            return {...state, items: action.payload, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case userActions.GET_USERWORKSTATIONS_FAILED: {
            return {...state, errorDetails: action.payload, severOperationtStatus: ServerOperationStatus.Fail};
        }

        // GET_SINGLE_USERWORKSTATION
        case userActions.GET_SINGLE_USERWORKSTATION: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case userActions.GET_SINGLE_USERWORKSTATION_SUCCESS: {
            return {...state, selectedUser: action.payload, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case userActions.GET_SINGLE_USERWORKSTATION_FAILED: {
            return {...state, errorDetails: action.payload, severOperationtStatus: ServerOperationStatus.Fail};
        }

        // SELECT_USERWORKSTATIONS
        case userActions.SELECT_USERWORKSTATIONS: {
            return {...state, selectedUser: action.payload};
        }

        // ADD_USERWORKSTATION
        case userActions.ADD_USERWORKSTATION: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case userActions.ADD_USERWORKSTATION_SUCCESS: {
            return {...state, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case userActions.ADD_USERWORKSTATION_FAILED: {
            return {...state, errorDetails: action.payload, severOperationtStatus: ServerOperationStatus.Fail};
        }

        // EDIT_USERWORKSTATION
        case userActions.EDIT_USERWORKSTATION: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case userActions.EDIT_USERWORKSTATION_SUCCESS: {
            return {...state, errorDetails: null, selectedUser: action.payload, severOperationtStatus: ServerOperationStatus.Success};
        }
        case userActions.EDIT_USERWORKSTATION_FAILED: {
            return {...state, errorDetails: action.payload, severOperationtStatus: ServerOperationStatus.Fail};
        }

        default: {
            return state;
        }
    }
}
