import * as activityActions from '../actions/activity.actions';
import { ActivityState } from '../models/activity-state';
import { ServerOperationStatus } from '../models/server-operation-status.enum';

const defaultActivityState: ActivityState = {
    severOperationtStatus: ServerOperationStatus.None
};

export function activityReducer(
    state: ActivityState = defaultActivityState,
    action: activityActions.SearchActivitiesActionType): ActivityState {
    switch (action.type) {
        case activityActions.SEARCH_ACTIVITIES: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case activityActions.SEARCH_ACTIVITIES_SUCCESS: {
            return {...state, items: action.payload, severOperationtStatus: ServerOperationStatus.Success};
        }
        case activityActions.SEARCH_ACTIVITIES_FAILED: {
            return {...state, errorDetails: action.payload, severOperationtStatus: ServerOperationStatus.Fail};
        }
        default: {
            return state;
        }
    }
}
