import * as scheduleActions from '../actions/schedule.actions';
import { ScheduleState } from '../models/schedule-state';
import { ServerOperationStatus } from '../models/server-operation-status.enum';

const defaultScheduleState: ScheduleState = {
    severOperationtStatus: ServerOperationStatus.None
};

export function scheduleReducer(
    state: ScheduleState = defaultScheduleState,
    action: scheduleActions.ScheduleActionType): ScheduleState {
    switch (action.type) {
        // GET_USER_WEEKLY_SCHEDULE
        case scheduleActions.GET_USER_WEEKLY_SCHEDULE: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case scheduleActions.GET_USER_WEEKLY_SCHEDULE_SUCCESS: {
            return {...state, schedule: action.payload, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case scheduleActions.GET_USER_WEEKLY_SCHEDULE_FAILED: {
            return {...state, errorDetails: action.payload, schedule: null, severOperationtStatus: ServerOperationStatus.Fail};
        }

        // GET_USER_OVERRIDE_SCHEDULE
        case scheduleActions.GET_USER_OVERRIDE_SCHEDULE: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case scheduleActions.GET_USER_OVERRIDE_SCHEDULE_SUCCESS: {
            return {...state, scheduleOverride: action.payload, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case scheduleActions.GET_USER_OVERRIDE_SCHEDULE_FAILED: {
            return {...state, errorDetails: action.payload, scheduleOverride: null, severOperationtStatus: ServerOperationStatus.Fail};
        }

        // EDIT_USER_WEEKLY_SCHEDULE
        case scheduleActions.EDIT_USER_WEEKLY_SCHEDULE: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case scheduleActions.EDIT_USER_WEEKLY_SCHEDULE_SUCCESS: {
            return {...state, schedule: action.payload, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case scheduleActions.EDIT_USER_WEEKLY_SCHEDULE_FAILED: {
            return {...state, errorDetails: action.payload, schedule: null, severOperationtStatus: ServerOperationStatus.Fail};
        }

        // EDIT_USER_OVERRIDE_SCHEDULE
        case scheduleActions.EDIT_USER_OVERRIDE_SCHEDULE: {
            return {...state, severOperationtStatus: ServerOperationStatus.InProgress};
        }
        case scheduleActions.EDIT_USER_OVERRIDE_SCHEDULE_SUCCESS: {
            return {...state, scheduleOverride: action.payload, errorDetails: null, severOperationtStatus: ServerOperationStatus.Success};
        }
        case scheduleActions.EDIT_USER_OVERRIDE_SCHEDULE_FAILED: {
            return {...state, errorDetails: action.payload, scheduleOverride: null, severOperationtStatus: ServerOperationStatus.Fail};
        } 

        default: {
            return state;
        }
    }
}
