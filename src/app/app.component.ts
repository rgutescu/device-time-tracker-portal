import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { RoutingService } from './core/services/routing.service';
import { NavLink } from './shared/models/nav-link';
import { AppState } from './state/models/app-state';
import { LayoutState } from './state/models/layout-state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'device-time-tracker';

  navBarBannerLabel: string = '';
  mainNavBarLinks: NavLink[];
  activeLink: NavLink;

  constructor(
    private routingService: RoutingService,
    private store: Store<AppState>) { }

  ngOnInit(): void {
      this.store.select(state => state.layoutData)
      .subscribe(layoutData => this.updateLayoutData(layoutData));
  }

  private updateLayoutData(layoutData: LayoutState) {
    this.mainNavBarLinks = layoutData.mainNavBarLinks;

    this.activeLink = this.mainNavBarLinks.find(link => link.isActive)[0];

    this.navBarBannerLabel = layoutData.navigationMessage;
  }
}
