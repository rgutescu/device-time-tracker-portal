export interface NavLink {
    routeLink: string;
    label: string;
    isActive: boolean;
}
