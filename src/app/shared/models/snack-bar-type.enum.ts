export enum SnackBarType {
    None = 0,
    Success = 1,
    Error = 2,
    Warning = 3
}
