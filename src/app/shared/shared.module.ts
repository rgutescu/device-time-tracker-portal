import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from './material.module';
import { LogoComponent } from './logo/logo.component';
import { NavBannerComponent } from './nav-banner/nav-banner.component';
import { LoaderComponent } from './loader/loader.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TimeFormatPipe } from './pipes/time-format.pipe';
@NgModule({
  declarations: [NavBarComponent, LogoComponent, NavBannerComponent, LoaderComponent, PageNotFoundComponent, TimeFormatPipe],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  exports: [
    NavBarComponent,
    LogoComponent,
    NavBannerComponent,
    LoaderComponent,
    PageNotFoundComponent,
    TimeFormatPipe
  ]
})
export class SharedModule { }
