import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'trk-nav-banner',
  templateUrl: './nav-banner.component.html',
  styleUrls: ['./nav-banner.component.scss']
})
export class NavBannerComponent implements OnInit {

  constructor() { }

  @Input() label: string;

  ngOnInit(): void {
  }

}
