import { Component, Input, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { NavLink } from '../models/nav-link';

@Component({
  selector: 'trk-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor() { }

  @Input() links: NavLink[];
  background: ThemePalette = 'primary';
  color: ThemePalette = 'primary';
  @Input() activeLink: NavLink;

  ngOnInit(): void {

  }
}
