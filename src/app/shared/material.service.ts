import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { SnackBarType } from './models/snack-bar-type.enum';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  constructor(private snackBar: MatSnackBar) { }

  showSnackBar(message: string, type: SnackBarType = SnackBarType.None, action: string = null, duration: number = 2000) : void {
    let cssPanelClass = null;

    switch (type) {
      case SnackBarType.Error: {
        cssPanelClass = 'snack-error';
        break;
      }
      case SnackBarType.Warning: {
        cssPanelClass = 'snack-warning';
        break;
      }
    }
    
    const config: MatSnackBarConfig = {
      duration: duration,
      panelClass: cssPanelClass
    };

    this.snackBar.open(message, action, config);
  }
}
