import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(value: number): string {
    const hours = Math.floor(value / 60);
    const minutes = value - (60 * hours);
    return (hours > 0) ? `${hours}h ${minutes}m ` : `${value} m`;
  }
}
