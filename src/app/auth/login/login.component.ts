import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MaterialService } from 'src/app/shared/material.service';
import { SnackBarType } from 'src/app/shared/models/snack-bar-type.enum';
import { AuthService } from '../auth.service';

@Component({
  selector: 'trk-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    private materialService: MaterialService,
    ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      pinCode: new FormControl(''),
    });
  }

  loginClicked() {
    const chosenPinCode = this.getPinCode();
    this.authService.login(chosenPinCode)
      .subscribe(() => {
        if (this.authService.isLoggedIn) {

          this.materialService.showSnackBar('Login successfull');

          // Redirect the user
          const redirectUrl = this.authService.redirectUrl || '/';

          this.router.navigate([redirectUrl]);
        }
        else {
          this.materialService.showSnackBar('Login failed', SnackBarType.Error);
        }
    });
  }

  logoutClicked() {
    this.authService.logout();
  }

  cancelLoginClicked() {
    this.router.navigate(['activity']);
  }

  get isLogged(): boolean {
    return this.authService.isLoggedIn;
  }

  private getPinCode(): string {
    return this.loginForm.value.pinCode;
  }
}
