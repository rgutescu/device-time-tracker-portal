import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _isLoggedIn: boolean = false;
  
  // store the URL so we can redirect after logging in
  private _redirectUrl: string;

  constructor() { }

  get isLoggedIn(): boolean {
    return this._isLoggedIn;
  }

  get redirectUrl() {
    return this._redirectUrl;
  }

  set redirectUrl(value: string) {
    this._redirectUrl = value;
  }

  login(code: string): Observable<boolean> {
    // TODO: call API to validate the code is (112)
    const isValid = code === '4989';
    
    return of(isValid).pipe(
      delay(250),
      tap(() => this._isLoggedIn = isValid)
    );
  }

  logout(): void {
    this._isLoggedIn = false;
  }
}
