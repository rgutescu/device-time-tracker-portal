import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityMainComponent } from './activity-main/activity-main.component';
import { ActivityRoutingModule } from './activity-routing.module';
import { ActivityFilterComponent } from './activity-filter/activity-filter.component';
import { ActivityListComponent } from './activity-list/activity-list.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../shared/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ActivityMainComponent, ActivityFilterComponent, ActivityListComponent],
  imports: [
    CommonModule,
    ActivityRoutingModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    ActivityMainComponent
  ]
})
export class ActivityModule { }
