import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/models/app-state';
import { ActivitySearchCriteria } from '../models/activity-search-criteria';
import { UserWorkstation } from '../models/user-workstation';

import * as activityActions from '../../state/actions/activity.actions';

import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'trk-activity-filter',
  templateUrl: './activity-filter.component.html',
  styleUrls: ['./activity-filter.component.scss']
})
export class ActivityFilterComponent implements OnInit {
  isLargeResolution: boolean;

  constructor(
    private store: Store<AppState>,
    public breakpointObserver: BreakpointObserver) { }

  @Input() userWorkstations: UserWorkstation[];
  @Input() startDate: Date;
  @Input() endDate: Date;

  selectedValue: UserWorkstation;
  filterForm: FormGroup;

  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(min-width: 800px)'])
      .subscribe((state: BreakpointState) => {
        this.isLargeResolution = state.matches;
    });
    
    this.selectedValue = this.userWorkstations[0];

    this.filterForm = new FormGroup({
      startDate: new FormControl(this.startDate),
      endDate: new FormControl(this.endDate),
      user: new FormControl(this.selectedValue.id)
    });

    // perform first search with default values
    this.performSearch();
  }

  performSearch(): void {
    const searchCriteria: ActivitySearchCriteria = {
      userId: this.filterForm.value.user,
      startDate: this.filterForm.value.startDate.toISOString(),
      endDate: this.filterForm.value.endDate.toISOString()
    };

    this.store.dispatch(new activityActions.SearchActivitiesAction(searchCriteria));
  }

  clearSearch(): void {
    const clearSearchCriteria: ActivitySearchCriteria = {
      userId: null,
      startDate: null,
      endDate: null
    };

    this.filterForm.patchValue({
      user: null,
      startDate: null,
      endDate: null
    });

    this.store.dispatch(new activityActions.SearchActivitiesAction(clearSearchCriteria));
  }
}
