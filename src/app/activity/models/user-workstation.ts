export interface UserWorkstation {
    id: string;
    userName: string;
    workstation: string;
    active: boolean;
}
