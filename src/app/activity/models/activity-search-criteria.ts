export interface ActivitySearchCriteria {
    userId: string;
    startDate: string;
    endDate: string;
}
