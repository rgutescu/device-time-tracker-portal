export interface ActivityDetails {
    userId: string;
    userName: string;
    workstation: string;
    startTimestamp: string;
    endTimestamp: string;
    duration: number;
}
