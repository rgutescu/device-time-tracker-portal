import {OnInit, AfterViewInit, Component, ViewChild, Input} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ActivityDetails } from '../models/activity-details';
@Component({
  selector: 'trk-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})
export class ActivityListComponent implements OnInit, AfterViewInit  {

  constructor() { }

  @Input() activityDetailsList: ActivityDetails[];

  displayedColumns: string[] = ['userName', 'workstation', 'startTimestamp', 'endTimestamp', 'duration'];
  dataSource: MatTableDataSource<ActivityDetails>;

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this.dataBind();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  getTotalDuration(): number {
    if (!this.activityDetailsList) {
      return 0;
    }

    return this.activityDetailsList
      .map(a => a.duration)
      .reduce((accumulatedValue, currentValue) => accumulatedValue +  currentValue, 0);
  }

  private dataBind(): void {
    this.dataSource = new MatTableDataSource(this.activityDetailsList);
  }
}
