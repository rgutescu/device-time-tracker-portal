import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ActivityMainComponent } from './activity-main/activity-main.component';

const routes: Routes = [
  {
      path: '',
      component: ActivityMainComponent,
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class ActivityRoutingModule { }
