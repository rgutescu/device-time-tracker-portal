import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/state/models/app-state';
import { ActivityDetails } from '../models/activity-details';
import { UserWorkstation } from '../models/user-workstation';

import * as userActions from '../../state/actions/user.actions';
import { ServerOperationStatus } from 'src/app/state/models/server-operation-status.enum';
import { getTodayDateWithoutTime, getTomorrowDateWithoutTime } from 'src/app/core/helpers/date-utils';

@Component({
  selector: 'trk-activity-main',
  templateUrl: './activity-main.component.html',
  styleUrls: ['./activity-main.component.scss']
})
export class ActivityMainComponent implements OnInit {

  constructor(private store: Store<AppState>) {
    this.userWorkstations$ = this.store.select(state => state.userData.items);
    this.activityDetailsList$ = this.store.select(state => state.activityData.items);

    this.isUserDataLoading$ = this.store.select(state => state.userData.severOperationtStatus);
    this.isActivityDataLoading$ = this.store.select(state => state.activityData.severOperationtStatus);
   }

  isUserDataLoading$: Observable<ServerOperationStatus>;
  isActivityDataLoading$: Observable<ServerOperationStatus>;

  userWorkstations$: Observable<UserWorkstation[]>;
  startDate: Date;
  endDate: Date;

  activityDetailsList$: Observable<ActivityDetails[]>;
  
  serverOperationInProgressStatus = ServerOperationStatus.InProgress;

  ngOnInit(): void {
    this.initializeFilterData();
  }  

  private initializeFilterData(): void {
    this.store.dispatch(new userActions.GetUserWorkstationsAction());

    this.startDate =  getTodayDateWithoutTime();
    this.endDate = getTomorrowDateWithoutTime();
  }
}
