import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { DashboardRoutingModule } from './dashboard-routing.module';



@NgModule({
  declarations: [DashboardMainComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ],
  exports: [
    DashboardMainComponent
  ]
})
export class DashboardModule { }
