# Device Time Tracker Portal
> `Device Time Tracker Portal` is the place of visualizing and configuring the data recorded by `Device Time Tracer Service`

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
Device Time Tracker Portal is a user-friendly visual interpretation of the data and functionality exposed by [Device Time Tracker Service](https://gitlab.com/rgutescu/devicetimetracker)
Main purpose of the product is to track, analyze and provide parental control for any kind of users.
Functionality of this service is bases on REST API, so it can be used from any external device 

## Technologies
* Angular 11
* Typescript
* NgRx (for state management)

## Setup
To run locally, requires instalation of Node.js and of Angular CLI 

To get more help on how to run the application using Angular CLI, use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Features
* Activity - visualize device activity for a specific user and interval of time
* Users - visualize and modify user data, disable user
* Schedule - visualize and modify weekly schedule data, for a specific user
* Schedule override - visualize and oerride daily schedule data, for a specific user

## To-do list:
* Create a Dashboard view - aggregated activity statistics (monthly, yearly)
* Implement permisison-based features, from service level
* Extend administrative capabilities

## Status
Project is: _in progress_, MVP functionality is done, but additional reporting capabilities are needed.

## Inspiration
Inspired by the need to have a more granular visibility of the time spent by children at the computer 

## Contact
Created by [@rgutescu]